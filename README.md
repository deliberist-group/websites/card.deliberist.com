# Business Card

[![pipeline status](https://gitlab.com/deliberist-group/websites/card.deliberist.com/badges/main/pipeline.svg)](https://gitlab.com/deliberist-group/websites/card.deliberist.com/-/commits/main)

It's important to note that the business card is in the
[index.html](https://gitlab.com/deliberist-group/websites/card.deliberist.com/-/blob/main/public/index.html)
but it is not a true HTML file, obviously.  But since we do not want an
unnecessarily long URL we need the file to be called `index.html`.

## Viewing

You can view the business card via cURL.  But since this is hosted with GitLab
Pages we need to follow rediects.

```bash
curl -L https://card.deliberist.com/
```

## Custom Text.

For fancy ASCII text, I used [textfancy.com](https://textfancy.com/) to create
custom text.

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.
